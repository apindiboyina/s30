<?php
class S30CheckLogin_DB  extends DatabaseModel {
	private $_siteID;
	private $_groupId;
	private $_sql = null;
	private $_table = 's30_site_configure';
	private $_result=null;
	//private $_table = 'incentive_details';
	
	public function __construct($siteid,$groupid) {
		if(!is_numeric($siteid))
			throw new Exception("SiteID is not numeric",500);
		
		parent::__construct(DB_HOST,DB_USER,DB_PASS,DB_DATABASE,$siteid);
		$this->_siteID = $this->escapeString($siteid);
		$this->_groupId = $this->escapeString($groupid);
		$this->_sql = array (
			$this->selectQuery(array("*"), $this->_table, "siteid = ".$this->_siteID." and groupid = ".$this->_groupId)
		);
	}
	
	public function GetSiteInfo() {
		if($this->_result==null)
		$this->_result = $this->runQuery($this->_sql[0]);
		return $this->_result;
	}  
}