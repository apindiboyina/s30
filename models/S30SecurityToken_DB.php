<?php
class S30SecurityToken_DB  extends DatabaseModel {	
	private $_groupId;
	private $_sql = null;
	private $_table = 's30_security_token';
	private $_result=null;
	//private $_table = 'incentive_details';
	
	public function __construct() {		
		parent::__construct(DB_HOST,DB_USER,DB_PASS,DB_DATABASE);		
	}
	
	public function CheckToken($token) {
		
		$sql = $this->selectQuery(array("token_value"), $this->_table,"token_value = '".$token."'");
		//print_r($sql);
		$res = $this->runQuery($sql);
		//print_r($res);
		
		return $res;
	}
	
	Public function StoreData($data) {	
		$insert = $this->insertQuery($data, $this->_table);
		$this->runQuery($insert);
		return $this->dbInsertID();
	}
}