<?php
class S30UserInformation_DB  extends DatabaseModel {
	private $_siteId;
	private $_groupId;
	private $_sql = null;
	private $_table = 's30_user_information';
	private $_result=null;
	//private $_table = 'incentive_details';
	
	public function __construct($emailid,$groupid) {		
		parent::__construct(DB_HOST,DB_USER,DB_PASS,DB_DATABASE);
		$this->_emailId = $this->escapeString($emailid);
		$this->_groupId = $this->escapeString($groupid);
		$this->_sql = array (
			$this->selectQuery(array("*"), $this->_table, "email = '".$this->_emailId."' and groupid = ".$this->_groupId)
		);
	}
	
	public function GetUserInfo() {
		if($this->_result==null)
		$this->_result = $this->runQuery($this->_sql[0]);
		return $this->_result;
	}
	
	public function StoreData($data) {		     
			$sql = $this->insertQuery($data, $this->_table);
  			$this->runQuery($sql);
                       return mysql_insert_id();
	}
	
	public function UpdateData($data) {
		
			$sql = $this->updateQuery($data, $this->_table, array('email'=>$this->_emailId,'groupid'=>$this->_groupId));
  			$this->runQuery($sql);
	}
}