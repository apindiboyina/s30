<?php
class S30UserInformation {
	
	public static function GetUserInfo($emailid,$groupid) {
		$s30UserInfo = new S30UserInformation_DB($emailid, $groupid);

		$s30UserData = $s30UserInfo->GetUserInfo();

		//include_once  BASE_DIR.'/views/widget.php';
		return $s30UserData;
	}
	
	public static function InsertData($data,$emailid,$groupid){
		$s30UserInfo = new S30UserInformation_DB($emailid, $groupid);
		$s30User = $s30UserInfo->StoreData($data);
		return $s30User;		
	}

	public static function SetData($data,$emailid,$groupid){
		$s30UserInfo = new S30UserInformation_DB($emailid, $groupid);
		$s30User = $s30UserInfo->UpdateData($data);
			
	}
}