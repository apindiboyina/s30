<?php
class ApiResponse
{
	public static function SendResponseJson($data,$crossdomain=true)
	{
		//$errorData = array('errorCode'=>0,'errorMsg'=>"No Errors");
		//$data = array_merge($data,$errorData);
		$json = json_encode($data);
		if($json === false)
			throw new Exception("Json Encoding failed while sending response.",500);
		if(isset($_GET['callback']))
			header('Content-type: text/javascript');
		else 
			header('Content-type: application/json');
		self::SendResponse($json, $crossdomain);
		
	}
	
	public static function SendErrorResponseJson($data,$statusCode,$crossdomain=true)
	{
		$json = json_encode($data);
		if($json === false)
			throw new Exception("Json Encoding failed while sending response.",6);
		header('Content-type: application/json');
		header("HTTP/1.0 ".$statusCode." ".httpstatuscodes::getMessage($statusCode));
		self::SendResponse($json, $crossdomain);
	}
	
	public static function SendResponseJSONP($data)
	{
		if(is_array($data))
			$data = json_encode($data);
		header("Content-type: text/javascript");
		self::SendResponse($data,false);
	}
	
	public static function SendResponsePlain($response,$crossdomain=true)
	{
		self::SendResponse($response, $crossdomain);
	}
	
	private static function SendResponse($response,$crossdomain=false)
	{
		if($crossdomain)
		{
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: POST, GET');
			header('p3p: CP="ALL DSP COR PSAa PSDa OUR NOR ONL UNI COM NAV"');
		}
		header('Cache-Control: no-cache,no-store');
		if(isset($_GET['callback']))
			$response = $_GET['callback']."(".$response.")";
			
		exit($response);
	}
	
	public static function Redirect($location)
	{
		header('Location: '.$location);
		exit();
	}
	
}