<?php
class CookieName {	
	public static function Get($name,$encrypted = true) {
		if($encrypted)
			$name = md5($name);
		return (isset($_COOKIE[$name]))? $_COOKIE[$name]:null;
	}
	
	public static function Set($name, $value,$expiry_secs,$encrypted = true){
		// crypt the cookies by md5 the names of cookies so that they cannot be identified by user for alteration.
		if($encrypted)
			$name = md5($name);
		$expiry_secs = intval($expiry_secs);
		$expiry_secs =  ($expiry_secs < 1)?null:time()+$expiry_secs; // if expiry is not passed then create a session cookie		
		setcookie($name,$value,$expiry_secs , "/", ".socialannex.com");		
	}
}