<?php
class S30UserInformationv2_DB  extends DatabaseModel
{
        private $_ucidVal;
	private $_groupId;
	private $_sql = null;
	private $_table = 's30_user_information';
	private $_result=null;
	//private $_table = 'incentive_details';
	
	public function __construct($groupid,$ucidVal) {		
		parent::__construct(DB_HOST,DB_USER,DB_PASS,DB_DATABASE);
		$this->_ucidVal= $this->escapeString($ucidVal);
		$this->_groupId = $this->escapeString($groupid);
		$this->_sql = array (
			$this->selectQuery(array("*"), $this->_table, " groupid= ".$this->_groupId." and id= ".$this->_ucidVal)
		);
	}
	
	public function GetUserInfo() {
		if($this->_result==null)
		$this->_result = $this->runQuery($this->_sql[0]);
		return $this->_result;
	}
}