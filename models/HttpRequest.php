<?php
class HttpRequest
{
	public static function GET($url,$queryParams)
	{
		if(!is_array($queryParams))
			throw new Exception("query params not passed as array.",500);
		$urlWithQueryparams = $url."?".http_build_query($queryParams);
		return file_get_contents($urlWithQueryparams);
	}
	
	public static function POST($url,$postParams)
	{
		if(!is_array($postParams))
			throw new Exception("post params not an array.",500);
		
		$data = http_build_query($postParams);
		
		$context = stream_context_create(
				array('http' =>
						array(
								'method' => 'POST',
								'header'=>"Content-type: application/x-www-form-urlencoded\r\n",
								'content'=>$data
						)
				)
		);
		return file_get_contents($url,false, $context);
	}
}