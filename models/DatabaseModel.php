<?php
class DatabaseModel {
  private $_dbconnection = NULL;
  
  public function __construct($host, $username, $password, $database) {
     
    if (empty ( $host ) or empty ( $username ) or empty ( $password ) or empty ( $database ))
      throw new Exception ( "Invalid params provided to DatabaseModel", 500 );
    
    $this->_dbconnection = mysql_connect ( $host, $username, $password );
    if (! $this->_dbconnection)
      throw new Exception ( "DB Connection Failed", 500 );
    mysql_select_db ( $database, $this->_dbconnection );
    mysql_set_charset ( "utf8", $this->_dbconnection );
  /*  if (! DISABLE_REDIS)
      $this->redis = new Predis_Client ( $this->redisConfig );*/
    //$this->siteID = $this->escapeString ( $siteid );
  }
  public function insertQuery($data, $table) {
    if (! is_array ( $data ))
      throw new Exception ( "in function insertQuery param1 is not an array", 500 );
    
    $cols = "(" . implode ( ",", array_keys ( $data ) ) . ") ";
    $sql = "INSERT INTO ";
    $sql .= mysql_real_escape_string ( $table, $this->_dbconnection ) . " ";
    $sql .= $cols;
    $sql .= " VALUES ";
    $sql .= "(";
    foreach ( $data as $field )
      $sql .= "'" . mysql_real_escape_string ( $field, $this->_dbconnection ) . "',";
    $sql = substr ( $sql, 0, - 1 ); // remove the last comma
    $sql .= ")";
    return $sql;
  }
  
	public function dbInsertID()
	{
		return mysql_insert_id();
	}
 public function updateQuery($data, $table, $where) {
		if (! is_array ( $data ) && ! is_array ( $where ))
			throw new Exception ( "in function updateQuery param1 and param3 are not array", 500 );
		
		$sql = "UPDATE " . $this->escapeString ( $table ) . " ";
		$sql .= "SET ";
		foreach ( $data as $col => $value ) {
			$sql .= $col . "='" . $this->escapeString ( $value ) . "', ";
		}
		$sql = substr ( $sql, 0, - 2 ); // remove the last comma and white space
		
		if (! empty ( $where )) {
			$sql .= " WHERE ";
			foreach ( $where as $col => $value )
				$sql .= $col . "='" . $this->escapeString ( $value ) . "' AND ";
			$sql = substr ( $sql, 0, - 5 );
		}
		return $sql;
	}
  public function selectQuery($columns, $table,$where = NULL,$alise=null,$join=null) {
    if (! is_array ( $columns ))
      throw new Exception ( "in function selectQuery param1 is not array", 500 );
		$sql = "SELECT " . implode ( ",", $columns ) . " FROM " . $table;	 
	  if($alise) 
	  $sql .= " AS ".$alise." ";
	  if($join)
	  $sql .= $join;
    if ($where)
      $sql .= " WHERE " . $where;
    return $sql;
   }

  public function runQuery($sql) {
    // check if this query is read query or write query
    $readQuery = strpos ( $sql, "SELECT" );
    $key = "S30Queries:" . md5 ( $sql );
   
    $result = mysql_query ( $sql, $this->_dbconnection );
    
    if ($result === false)
      throw new Exception ( "DB Query Failed, MSG:" . mysql_error ( $this->_dbconnection ), 500 );
    if ($result === true)
      return $result;
    
    $returnArray = array ();
    while ( $row = mysql_fetch_array ( $result, MYSQL_ASSOC ) ) {
      $returnArray [] = $row;
    }
    
    mysql_free_result ( $result );   
   
    return $returnArray;
  }
  public function runQueryUncached($sql) {
    $result = mysql_query ( $sql, $this->_dbconnection );
    if ($result === false)
      throw new Exception ( "DB Query Failed, MSG:" . mysql_error ( $this->_dbconnection ), 500 );
    if ($result === true)
      return $result;
    
    $returnArray = array ();
    while ( $row = mysql_fetch_array ( $result, MYSQL_ASSOC ) ) {
      $returnArray [] = $row;
    }
    // var_dump($returnArray);
    mysql_free_result ( $result );
    return $returnArray;
  }
  public function escapeString($unescaped_string) {
    return mysql_real_escape_string ( $unescaped_string, $this->_dbconnection );
  }
 
  function __destruct() {
    /*
     * if($this->_dbconnection) mysql_close($this->_dbconnection);
     */
  }
}