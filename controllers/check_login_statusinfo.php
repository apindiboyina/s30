<?php
require dirname(__FILE__).'/../config.php';
$groupid=$_GET['groupid'];
$ucidVal = CookieName::Get("ucid");
//echo "value==".$cookieVal;
$data = "null"; //[AA] passing null as string converts the data to javascript null
if($ucidVal)
{
  $check_login=S30UserInformationv2::GetUserInfo($groupid,$ucidVal);
  //echo "login==";print_r($check_login);
  $check_logindata=array(
		"firstname" =>$check_login[0]['first_name'],
		"lastname" =>$check_login[0]['last_name'],
		"email" =>$check_login[0]['email'],
                "siteuid" =>$check_login[0]['siteuid'],
		"dob" =>$check_login[0]['dob'],
                "gender" =>$check_login[0]['gender'],
		"address" =>$check_login[0]['address'],
		"city" =>$check_login[0]['city'],
		"state" =>$check_login[0]['state'],
                "provider" =>$check_login[0]['provider'],
		"country" =>$check_login[0]['country']
	);
  $userInfoJson = json_encode($check_logindata);
  $userInfo = base64_encode($userInfoJson);
  $data = array($userInfo);
  $date = date('Y/m/d H:i:s');
}
ApiResponse::SendResponseJSONP($data);