<?php
require dirname(__FILE__).'/../config.php';

$emailid = $_GET['emailid'];
$signature = $_GET['signature'];
$secondarykey = $_GET['sec_key'];
$groupid = $_GET['groupid'];
$siteid = $_GET['siteid'];

$check_login=S30CheckLoginStatus::CheckLoginStatus($siteid,$groupid);
$groupid=$check_login[0]['groupid'];
$secret_key=$check_login[0]['secret_key'];
$sig=base64_encode(hash_hmac("sha256",$secondarykey, $secret_key, true));

if($signature!=$sig) //if signature not valid throw error
  throw new Exception("Auth error: invalid signature",401);

$checkToken=S30SecurityToken::CheckSecurityToken($signature);

if($checkToken == true)//if token already used throw error
  throw new Exception("Auth error: token used",401);

$cookieVal = CookieName::Get($groupid);
//echo "value==".$cookieVal;
$data = "null"; //[AA] passing null as string converts the data to javascript null
if($cookieVal)
{
  $check_login=S30UserInformation::GetUserInfo($emailid,$groupid);
  //echo "login==";print_r($check_login);
  $userInfoJson = json_encode($check_login);
  $userInfo = base64_encode($userInfoJson);
  $data = array($userInfo);
  $date = date('Y/m/d H:i:s');
  $token_data=array(
      "token_value"=> $signature,
      "ss_uid"=>$check_login[0]['ss_uid'],
      "groupid" => $groupid,
      "db_add_date" => $date
  );
  S30SecurityToken::StoreSecurityToken($token_data);
}

ApiResponse::SendResponseJSONP($data);