<?php
ob_start();
require dirname(__FILE__).'/../config.php';
$user = $_GET['user'];
$signature = $_GET['signature'];
$secondaryKey = $_GET['secondary_key'];
$groupid = $_GET['groupid'];
$siteid = $_GET['siteid'];
//$sessionExpiry = json_decode($_GET['sessionExpiry']);
$sessionExpiry = $_GET['sessionExpiry'];
$userInfo = base64_decode($user);
$userInformation = json_decode($userInfo);
$emailid=$userInformation->email;

$check_login=S30CheckLoginStatus::CheckLoginStatus($siteid,$groupid);
$secret_key=$check_login[0]['secret_key'];
$group_name=$check_login[0]['group_name'];
$sig=base64_encode(hash_hmac("sha256",$secondaryKey, $secret_key, true));

if($signature!=$sig) //if signature not valid throw error
  throw new Exception("Auth error: invalid signature",401);


$checkToken=S30SecurityToken::CheckSecurityToken($signature);

if($checkToken == true)//if token already used throw error
  throw new Exception("Auth error: token used",401);

$user=S30UserInformation::GetUserInfo($emailid,$groupid);	
$data=null;
$getlastID=null;
$getlastinsertId=null;
if(!isset($user[0]['ss_uid']))
{
	$fu_random_id = uniqid (rand ());
	$date = date('Y/m/d H:i:s');
	$data=array(
		"ss_uid" =>$fu_random_id,
		"groupid" =>$groupid,
		"siteuid" =>$userInformation->siteuid,
		"siteid" =>$siteid,
		"first_name" =>$userInformation->firstname,
		"last_name" =>$userInformation->lastname,
		"email" =>$userInformation->email,
		"gender" =>$userInformation->gender,
		"dob" =>$userInformation->dob,
		"address" =>$userInformation->address,
		"city" =>$userInformation->city,
		"state" =>$userInformation->state,
		"country" =>$userInformation->country,
                "provider" =>$userInformation->provider,
		"db_add_date" =>$date
	);
	$getlastID=S30UserInformation::InsertData($data,$emailid,$groupid);	
}
if(!isset($user[0]['id']))
{
$getlastinsertId=$getlastID;
}
 else {
    $getlastinsertId=$user[0]['id'];
}
$userInfoJson = json_encode($user);
$data = md5(base64_encode($userInfoJson));

CookieName::Set($groupid, $group_name,$sessionExpiry);
CookieName::Set($group_name, $data,$sessionExpiry);
CookieName::Set("ucid",$getlastinsertId,$sessionExpiry);
ApiResponse::SendResponseJSONP("true");

