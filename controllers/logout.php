<?php
require dirname(__FILE__).'/../config.php';
require_once('lib/_autoload.php');
if(empty($_REQUEST['LogoutState']))
{
	$as = new SimpleSAML_Auth_Simple('default-sp');
	$idp = $as->getAuthData('saml:sp:IdP');
	$logoutURL=$as->logout(array(
		'ReturnTo' => 'http://www.simplesp.com/logout.php',
		'ReturnStateParam' => 'LogoutState',
		'ReturnStateStage' => 'MyLogoutState',
	));
}
else
{
		$state = SimpleSAML_Auth_State::loadState((string)$_REQUEST['LogoutState'], 'MyLogoutState');
		$ls = $state['saml:sp:LogoutStatus']; /* Only works for SAML SP */
		if ($ls['Code'] === 'urn:oasis:names:tc:SAML:2.0:status:Success' && !isset($ls['SubCode'])) {
			/* Successful logout. */
			echo("You have been logged out.");
		} else {
			/* Logout failed. Tell the user to close the browser. */
			echo("We were unable to log you out of all your sessions. To be completely sure that you are logged out, you need to close your web browser.");
		}
}
?>