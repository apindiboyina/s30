<?php
require dirname(__FILE__).'/../config.php';

$user = $_GET['user'];
$signature = $_GET['signature'];
$secondaryKey = $_GET['secondary_key'];
$groupid = $_GET['groupid'];
$siteid = $_GET['siteid'];
$userInfo = base64_decode($user);
$userInformation = json_decode($userInfo);

$emailid=$userInformation->email;

$check_login=S30CheckLoginStatus::CheckLoginStatus($siteid,$groupid);
$secret_key=$check_login[0]['secret_key'];
$sig=base64_encode(hash_hmac("sha256",$secondaryKey, $secret_key, true));

$cookieVal = CookieName::Get($groupid);
if($cookieVal==null)//if usert not login throw error
  throw new Exception("Auth error: user not logged in",401);

if($signature!=$sig) //if signature not valid throw error
  throw new Exception("Auth error: invalid signature",401);

$checkToken=S30SecurityToken::CheckSecurityToken($signature);

if($checkToken == true)//if token already used throw error
  throw new Exception("Auth error: token used",401);

$user=S30UserInformation::GetUserInfo($emailid,$groupid);

$data=null;
if(isset($user[0]['ss_uid']))
{
	$userid=$userInformation->siteuid;
	$date = date('Y/m/d H:i:s');
	$data=array(
		"first_name" =>$userInformation->firstname,
		"last_name" =>$userInformation->lastname,
		"email" =>$userInformation->email,
		"gender" =>$userInformation->gender,
		"dob" =>$userInformation->dob,
		"address" =>$userInformation->address,
		"city" =>$userInformation->city,
		"state" =>$userInformation->state,
		"country" =>$userInformation->country,
		"db_add_date" =>$date
	);
	//echo "user===".$userid;
	S30UserInformation::SetData($data,$emailid,$groupid);
	ApiResponse::SendResponseJSONP("true"); //send response as true if successfully saved the data
}
ApiResponse::SendResponseJSONP("false"); // if data was not saved send a false response
