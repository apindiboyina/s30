<?php 
require_once('lib/_autoload.php');
include_once('ApiResponse.php');
$as = new SimpleSAML_Auth_Simple('default-sp');
//We then require authentication:
if(!empty($_GET['site_id']) && !empty($_GET['groupId']))
{
		if ($as->isAuthenticated())
		{
			$session = SimpleSAML_Session::getSessionFromRequest();
			//---------------------set socialannex session name---------------//
			$attrs = $as->getAttributes();
			$cookie_sender_name= array(
                  'site_id'=>$_GET['site_id'],
				  'groupId'=>$_GET['groupId']
                  );
			$cookiesenderJson = json_encode($cookie_sender_name);
			$cookiesendername = base64_encode($cookiesenderJson);
			//---------------------set socialannex session value-------------------------------//
			$data = array(
                  'uid'=>$attrs['uid']['0'],
				  'firstname'=>$attrs['firstname']['0'],
				  'lastname'=>$attrs['lastname']['0'],
				  'Gender'=>$attrs['Gender']['0']
                  );
			$userInfoJson = json_encode($data);
			$userInfo = base64_encode($userInfoJson);
			setcookie($cookiesendername,$userInfo,time() +3600*24*1,"/",".socialannex.com");
			//-----------------------end-----------------//
		    ApiResponse::SendResponseJson($userInfo);
		}
		else
		{
			 $errorInfo=array(
                  'Status'=>"failed",
				  'Message'=>"User data is empty"
                  );
				 $errorInfoJson = json_encode($errorInfo);
				$errorInfodata = base64_encode($errorInfoJson);
			ApiResponse::SendResponseJson($errorInfodata);
		}
}
else
{
 $errorInfo=array(
                  'Status'=>"failed",
				  'site_id'=>"Site id is empty",
				  'groupId'=>"group id is empty"
                  );
				 $errorInfoJson = json_encode($errorInfo);
				$errorInfodata = base64_encode($errorInfoJson);
 ApiResponse::SendResponseJson($errorInfodata);
}
?>