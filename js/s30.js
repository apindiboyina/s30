(function(w){
	var S30Obj = {
		siteid: null,
		config: null,
		user:null,
		initOptions:null,
		groupid:null,
		intialized:false,
		init: function(options){
			if(!this.checkInitParams(options)){
				this.printc("Init params are incorrect, aborting single signon.");
				return;
			}
			this.initOptions = options;
			this.siteid = this.initOptions.siteId;
			this.groupid = this.initOptions.groupId;
			this.intialized = true;
		},
		checkInitParams : function (params){
			if(typeof(params.siteId) != 'number' && typeof(params.groupId) != 'number') //check if init params are correct format
				return false;
			return true;
		},
		checkState : function(options,callback){
			if(!this.intialized)
				return;
			var emailid = options['emailId'];
			var signature = options['signature'];
			var secondary_key = options['secondary_key'];
			var url = "//www.socialannex.com/s30/controllers/check_login_status.php?siteid="+this.siteid+"&groupid="+this.groupid+"&emailid="+emailid+"&signature="+signature+"&sec_key="+encodeURIComponent(secondary_key);
			this.getScript(url,function (data){ 
			callback(data);
			});	
		},
                checkLoginState : function(options){
			if(!this.intialized)
			return;
                        
                        var sa30redirectUri = options['redirectUri'];
			var url = "//www.socialannex.com/s30/controllers/check_login_statusinfo.php?groupid="+this.groupid;
			this.getScript(url,function (data)
                        { 
                            if(data==null)
                            {
                               window.location.href =sa30redirectUri;
                            }
			});	
		},
                checkhandleLoginState : function(options)
                {
			if(!this.intialized)
			return;
                        var sa30redirectUri = options['redirectUri'];
			var url = "//www.socialannex.com/s30/controllers/check_login_statusinfo.php?groupid="+this.groupid;
			this.getScript(url,function (data)
                        { 
                        var sa_separator="";
                        if(sa30redirectUri.indexOf('?')>0){sa_separator = "&";}else{sa_separator = "?";}    
			if(data!=null)
                        {
                        window.location.href =sa30redirectUri+""+sa_separator+"sas30code="+data;
                        }
			});	
		},
                
		getScript : function(url, callback) {
			var timestamp = new Date().getTime();
			var selfcallback = "s30callback_"+timestamp;
			url = url+"&callback="+selfcallback;
			w[selfcallback] = callback;
			
			var head = document.getElementsByTagName("head")[0];
		    var newScript = document.createElement("script");
		    newScript.type = "text/javascript";
		    newScript.src = url;
		    newScript.async = true;
		    head.appendChild(newScript);
		},
		notifyLogin : function(options){
			if(!this.intialized)
				return;
			
			this.user = options['user'];
			var signature = options['signature'];
			var secondaryKey = options['secondary_key'];
			var sessionExpiry = options['sessionExpiry'];
                        var Sa30redirectUri = options['redirectUri'];
			if(null == sessionExpiry) //if sessionExpiry is not set the make the cookie as session cookie
				sessionExpiry = -1;
			var url="//www.socialannex.com/s30/controllers/notify_login.php?user="+this.user+"&signature="+signature+"&secondary_key="+encodeURIComponent(secondaryKey)+"&groupid="+this.groupid+"&siteid="+this.siteid+"&sessionExpiry="+sessionExpiry;
			this.getScript(url,function (data)
                        {
                          if(data==true){window.location.href=Sa30redirectUri;}else{console.log("Login Faild:"+data);}
			});	
		},
		logout : function(options){
			if(!this.intialized)
				return;
			var Sa30redirectUri = options['redirectUri'];
			var url="//www.socialannex.com/s30/controllers/clear_session.php?groupid="+this.groupid+"&siteid="+this.siteid;
			this.getScript(url,function (data){
				if(data==true){window.location.href=Sa30redirectUri;}else{console.log("cache not clear");}
			});	
		},
		setUser : function (options){
			if(!this.intialized)
				return;
			this.user = options['user'];
			var signature = options['signature'];
			var secondaryKey = options['secondary_key'];
			var url="//www.socialannex.com/s30/controllers/store_user_information.php?user="+this.user+"&signature="+signature+"&secondary_key="+encodeURIComponent(secondaryKey)+"&groupid="+this.groupid+"&siteid="+this.siteid;
			this.getScript(url,function (data){ 
					console.log("Set User:"+data);
			});
		},
		printc:function(msg){
			if(window.console)
				console.log(msg);
		}
	};	 
	
	//use this function to let the main page know that the javascript has loaded.
	function fireS30Init()
	{
		if(typeof (w.s30AsyncInit) == 'function'){
			w.S30Obj = S30Obj;
			w.s30AsyncInit();
		}
	}
	   
	fireS30Init();
})(window);