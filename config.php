<?php
$env = getenv('DEVENV');
if(isset($env) && $env)
{
	define("DB_HOST", "localhost");	
}
else
{
	define("DB_HOST", "10.34.13.62");	
}

define("DB_USER", "socialan_soannex");
define("DB_PASS", "No,Cp.91;");
define("DB_DATABASE", "socialan_annex");

$dir = dirname(__FILE__);
define("BASE_DIR",$dir);
spl_autoload_register('myautoload');
//include custom files from sdk here


//print_r($twitterSDKInclude);exit();

function myautoload($class_name)
{
	$fileInCore = BASE_DIR.DIRECTORY_SEPARATOR."models".DIRECTORY_SEPARATOR.$class_name.".php";
	if(file_exists($fileInCore))
		require_once $fileInCore;
}

//handle exceptions
function handleExceptions($exception)
{
  ApiResponse::SendErrorResponseJson(array('errorMsg'=>$exception->getMessage()),$exception->getCode());
}

/*handle errors, some php cannot be handled with this function 
 * E_ERROR , 
 * E_PARSE , 
 * E_CORE_ERROR , 
 * E_CORE_WARNING ,  
 * E_COMPILE_ERROR ,
 * E_COMPILE_WARNING
 * above errors are not handled, and you will get no response from file, 
 * please check php error log to find these errors
 */

function handleErrors($errorNo,$errstr,$errfile,$errline)
{
	ApiResponse::SendErrorResponseJson(array('errorMsg'=>$errstr,'errorNo'=>$errorNo,'file'=>$errfile,'line'=>$errline), 500);
}

function handleShutdown()
{
	$error = error_get_last();
	if(null !== $error && ($error['type'] !== 8192)) //skip error 8192, its deprecated warning error
		handleErrors($error['type'], $error['message'], $error['file'], $error['line']);
}

ini_set('display_errors', 0); //hide display of errors , because we are sending errors as jsons back to requesting urls
set_exception_handler('handleExceptions');
set_error_handler('handleErrors', E_WARNING | E_NOTICE | E_STRICT | E_USER_WARNING);
register_shutdown_function('handleShutdown');

//check if we are able to connect to redis
/*
$redis = new Predis_Client(array(
    'host'=> REDIS_HOST,
    'port'=> REDIS_PORT,
));
try {
  $redis->connect();
  if(!defined('DISABLE_REDIS')){
    if(FORCE_DISABLE_REDIS)
      define("DISABLE_REDIS",TRUE); //even if connected force disable the redis server
    else
      define("DISABLE_REDIS",FALSE); //if connection is successfull use redis
  }
}
catch (Exception $e)
{
  if(!defined('DISABLE_REDIS'))
    define("DISABLE_REDIS",TRUE); //if connection is unsuccessfull dont use redis, directly use db
} 
*/
 